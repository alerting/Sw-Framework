<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/5
 * Time: 18:39
 * description:描述
 */

return [
    "host"=>env('app.tcphost', '0.0.0.0'),'port'=>env('app.tcpport', '9557'),
    "set"=>[
        "worker_num"=> env('app.swoole_worker_num',1),
        "task_worker_num"=> env('app.swoole_task_worker_num',1),
        "max_request"=>100,"task_max_request"=>100,"max_connection"=>10000,
        "daemonize"=>0, 'log_file'=>ROOT_PATH.'/runtime/log/'.env('app.swoole_log','swoole_log').date('Y-m-d').'.log',
        "package_eof"=>"|end|", "open_eof_check"=>true,'reload_async' => true,
        "backlog"=>10000,
    ],
    "masterProcessName"=>"LogCenter-master",
    "managerProcessName"=>"LogCenter-manager",
    "workerProcessName"=>"LogCenter-worker",
    "taskProcessName"=>"LogCenter-task",
    "masterPidFile"=>dirname(__DIR__).DIRECTORY_SEPARATOR.'master.pid',
    "managerPidFile"=>dirname(__DIR__).DIRECTORY_SEPARATOR.'manager.pid',
    "workerPidFile"=>dirname(__DIR__).DIRECTORY_SEPARATOR.'worker.pid',
    "taskPidFile"=>dirname(__DIR__).DIRECTORY_SEPARATOR.'task.pid',
];
