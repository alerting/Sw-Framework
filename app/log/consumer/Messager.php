<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/21
 * Time: 9:49
 * description:描述
 */

namespace App\log\consumer;


use App\common\exception\ConfigException;
use Lc\lib\redis\RedisPool;

class Messager implements Consumer {
    private $config;
    private $driver;
    private $redis;
    private $keyFilters = ['error'];

    public function __construct(array $config) {
        $this->config = $config;
        $driverName = 'App\log\consumer\messager\\' . $config['type'];
        if (!class_exists($driverName)) {
            throw new ConfigException('配置异常,消息驱动' . $driverName . '不存在');
        }
        $this->keyFilters = array_merge($this->keyFilters, $config['filter']['level']);
        $this->driver = new $driverName($config['groups']);
        $this->redis = RedisPool::getInstance()->get();
    }

    public function handle(array $message,string $topic) {
        // TODO: Implement handle() method.

        return $this->messageFilter($message, function ($message)use($topic) {
            $message = $this->messageFormater($message);
            $res=$this->driver->handle($message, $topic);
            return $res;
        });

    }

    private function messageFilter(array $message, \Closure $func) {

        if (!array_intersect(array_keys($message), $this->keyFilters)) {
            output( '该消息级别,被消费者过滤:' . $message['logId']);
            return ['status'=>true,'msg'=>'filted'];
        }
        $feature=(key_exists('uri', $message) ? $message['uri'] : 'uri') . (key_exists('param', $message) ?
                json_encode($message['param']) : 'param') . (key_exists('ip', $message) ? $message['ip'] :
                'ip');
        $featureCode = md5($feature);

        if ($this->redis->exists('logEvent:messager:filter:' . $featureCode)) {
            $this->redis->incr('logEvent:messager:filted');
            output("重复消息,已被消费者过滤!" . $message['logId']);
            return ['status'=>true,'msg'=>'filted'];
        }

        $this->redis->set('logEvent:messager:filter:'.$featureCode, true, $this->config['filter']['repeat']);

        return $func($message);
    }

    protected function messageFormater(array $content) {
        $message = "请求时间:" . $content['time'];
        $message .= "\n请求URI:" . $content['uri'];
        $message .= "\n异常:" . (is_array($content['error'])?json_encode($content['error'],JSON_UNESCAPED_UNICODE):$content['error']);
        $message .= "\n请求IP:" . $content['ip'];
        $message .= "\n服务IP:" . $content['serverIp'];
        $message .= "\n主题:" . $content['project'];
        $message .= "\nID:" . $content['logId'];
        unset($content['time']);
        unset($content['uri']);
        unset($content['error']);
        unset($content['ip']);
        unset($content['serverIp']);
        unset($content['logId']);
        unset($content['project']);

        $message .= "\n请求信息:" . arrayToStr($content);
        return $message;
    }

    public function __destruct() {
        // TODO: Implement __destruct() method.
        if ($this->redis)
            RedisPool::getInstance()->pushRedis($this->redis);
    }
}
