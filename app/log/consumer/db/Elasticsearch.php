<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 14:52
 * description:描述
 */

namespace App\log\consumer\db;

use App\log\consumer\db\Db;
use App\common\exception\WarringException;

class Elasticsearch extends Db {
    private $client;
    private $config;


    public function __construct(array $config) {

        $this->client = new \Swoole\Coroutine\Http\Client($config['elasticsearch']['host'], $config['elasticsearch']['port']);
        $this->client->setHeaders([
            'Content-Type' => 'application/json; charset=utf-8',
        ]);
        $this->client->set(['timeout' => 1.0]);
        $this->config=$config;

    }

    public function handle(array $data,string $topic) {
        $this->client->setMethod("POST");
        $this->client->setData(json_encode($data));

        $uri="/".$this->config['topicDbMap'][$topic].'/_doc';
        $this->client->execute($uri);
        $return=$this->client->body;
        output('ES请求:结果='.$return);
        $this->client->close();
        $res=json_decode($return,true);
        if(!is_array($res)||!key_exists('result',$res)||key_exists('error',$res)){
            throw new WarringException('ES添加日志失败:uri='.$uri.'信息='.json_encode($data).'结果='.$return);
        }else{
            $result=['status'=>true,'msg'=>'ok'];
        }
        return $result;
    }
}
