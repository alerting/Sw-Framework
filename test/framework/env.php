<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/6
 * Time: 11:27
 * description:描述
 */
namespace  Test\framework;

use PHPUnit\Framework\TestCase;
use Lc\lib\Env;
class EnvTest extends TestCase
{
    public function test()
    {
        $env=Env::getInstance()->load(dirname(dirname(__DIR__)).'/.env');


        $this->assertEquals('0.0.0.0', $env->get('LOGCENTER.TCPHOST'));

    }
}
