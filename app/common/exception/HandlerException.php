<?php

namespace App\common\exception;


class HandlerException
{

    public static function register()
    {
        error_reporting(E_ALL);
        set_error_handler([__CLASS__, 'appError']);
//        set_exception_handler([__CLASS__, 'appException']);
        register_shutdown_function([__CLASS__, 'appShutdown']);
    }

    public static function appError($errno = '', $errstr = '', $errfile = '', $errline = 0) {

        $err_str = "[{$errno}] 文件：{$errfile} 中第 {$errline} 行：{$errstr} ";
        reportLog($err_str);

    }

    public static function appShutdown() {

        if (!is_null($error = error_get_last()) && self::isFatal($error['type'])) {
            $err_str = "[{$error['type']}] 文件：{$error['file']} 中第 {$error['line']} 行：{$error['message']} ";
            reportLog($err_str);

        }
    }

    /**
     * 确定错误类型是否致命
     *
     * @access protected
     * @param  int $type
     * @return bool
     */
    protected static function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    protected static function logRecord($err_str = '') {
        $config = config();
        $error_log_path = $config['error_log_file'];
        $log_file_size = 0;
        if (is_file($error_log_path)) {
            $log_file_size = filesize($config['error_log_file']);
        }
        if ($log_file_size > 1024*20) { //超过20M进行清理
            file_put_contents($error_log_path,'');
        }
        $msg = "[".date("Y-m-d H:i:s")."] ".$err_str.PHP_EOL;
        file_put_contents($error_log_path, $msg, FILE_APPEND);
    }
}
