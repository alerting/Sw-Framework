<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/5
 * Time: 18:39
 * description:描述
 */

return [
    'pool_size'=>20,
    'pool_get_timeout'=>2,
    'host' => env('redis.master_hostname', '127.0.0.1'),
    'port' => env('redis.master_port', '6379'),
    'auth' => env('redis.master_auth', 'secret'),
];
