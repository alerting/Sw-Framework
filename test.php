<?php
class Solution1 {

    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function twoSum($nums, $target) {
        $return=[];
        foreach($nums as $k=>$v){
            foreach($nums as $key=>$value){
                if($v+$value==$target && $k!==$key){
                    $return =[$k,$key];
                    break 2;
                }
            }
        }
        return $return;
    }
}
class Solution {

    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function twoSum($nums, $target) {
        $return=[];
        $num=count($nums);
        foreach($nums as $k=>$v){
            $num=count($nums);
            for($i=$k+1; $i<$num;$i++){
                if($v+$nums[$i]==$target){
                    $return =[$k,$i];
                    break 2;
                }
            }
        }
        return $return;
    }
}


class Solution3 {
    function twoSum($nums, $target) {
        $hash = [];
        $n = count($nums);
        for ($i = 0; $i < $n; ++$i) {
            $diff = $target - $nums[$i];
            if (isset($hash[$diff])) return [$i, $hash[$diff]];
            $hash[$nums[$i]] = $i;
        }
        return [];
    }
}


$array=[3,2,4,8];
$target=6;
$solution=new Solution();
echo 'startTime'.microtime();
$result=$solution->twoSum($array,$target);

var_dump($result);
echo "\n".'endTime'.microtime();
