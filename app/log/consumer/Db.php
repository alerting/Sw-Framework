<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/21
 * Time: 9:49
 * description:描述
 */

namespace App\log\consumer;

use App\common\exception\ConfigException;
use Lc\lib\redis\RedisPool;

class Db implements Consumer {
    private $config;
    private $driver;
    private $keyFilters = ['error'];

    public function __construct(array $config) {
        $this->config = $config;
        $driverName = 'App\log\consumer\db\\' . $config['type'];
        if (!class_exists($driverName)) {
            throw new ConfigException('配置异常,db驱动' . $driverName . '不存在');
        }
        $this->driver = new $driverName($config);
        $this->redis = RedisPool::getInstance()->get();
    }

    public function handle(array $message, string $topic) {

        return $this->messageFilter($message, function ($message) use ($topic) {
            $message = $this->messageFormater($message);
            $re = $this->driver->handle($message, $topic);
            return $re;
        });

    }

    private function messageFilter(array $message, \Closure $func) {
        return $func($message);
    }

    protected function messageFormater(array $content) {

        foreach ($content as $key =>$value){
            if(!in_array($key,$this->config['protocol'])){
                $content['ext'][$key]=$value;
                unset($content[$key]);
            }
        }
        if (is_array($content['ext'])) {
            foreach ($content['ext'] as $key => $value) {
                $content['ext'][$key] = json_encode($value);
            }
        }
        return $content;
    }

    public function __destruct() {
        // TODO: Implement __destruct() method.
        if ($this->redis)
            RedisPool::getInstance()->pushRedis($this->redis);
    }
}
