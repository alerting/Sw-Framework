<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Lc\lib;

use Lc\lib\traits\SingletonTrait;

class Config {
    use SingletonTrait;
    /**
     * 环境变量数据
     * @var array
     */
    public $data = [];



    public function load(string $confDir){
        $data = [];
        foreach (scandir($confDir) as $dir) {
            $tmpPath = realpath($confDir . DIRECTORY_SEPARATOR . $dir);
            if ((!is_dir($tmpPath)) && pathinfo($tmpPath, 4) === 'php') {
                $data[pathinfo($dir, 8)] = require($tmpPath);
            }
        }
        $this->data = $data;
        return $this;
    }

    /**
     * 获取环境变量值
     * @access public
     * @param  string $name 环境变量名
     * @param  mixed  $default 默认值
     * @return mixed
     */
    public function get($name = null) {

//        echo json_encode($this->data);
        if (is_null($name)) {
            return $this->data;
        }

        $name = explode('.', $name);

        $config = $this->data;

        // 按.拆分成多维数组进行判断
        foreach ($name as $val) {
            if (isset($config[$val])) {
                $config = $config[$val];
            } else {
                return null;
            }
        }
        return $config;
    }



}
