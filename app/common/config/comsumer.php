<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/5
 * Time: 18:39
 * description:描述
 */

return [
    'messager'=>[
        'type'=> env('app.message_type','Ding'),
        'filter' =>['repeat'=>(int)env('app.repeat',120),
                    'level'=>explode(',',env('app.level','error,emergency,critical,alert,warning'))],
        "groups" => [
            'default' => ['enabled'  => true, 'token'    => env('app.messager_default_token','cb3e7d7e0471f87aa853737135d850994d60013e5442460ac57e31635d9d431f'),
                          'timeout'  => 2.0, 'ssl_verify' => false,
                          'keywords' => env('reporter.messager_default_keywords','勤鸟小伙伴,你好!'),],
        ],
    ],
    'db'=>[
        'type'=> env('app.db_type','Elasticsearch'),
        "topicDbMap" => ['veinopen' =>  env('ENVIRONMENT','test').'requestlog',
                         'messager' =>  env('ENVIRONMENT','test').'messager',
                         'veinVerify' =>  env('ENVIRONMENT','test').'veinverify',],
        'elasticsearch'=> [
            'host' => env('elasticsearch.es_host', '127.0.0.1'),
            'port' => env('elasticsearch.es_port', '9200'),
        ],
    ],
];
