<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 14:52
 * description:描述
 */

namespace App\log\consumer\messager;

use App\common\exception\WarringException;


class Ding extends Messager {
    private $client;
    private $config;
    private $gate = "oapi.dingtalk.com";

    public function __construct(array $config) {
        $this->client = new \Swoole\Coroutine\Http\Client($this->gate,443);
        $this->client->setHeaders([
            'Content-Type' => 'application/json; charset=utf-8',
        ]);
        $this->client->set(['timeout' => 1.0]);
        $this->config = $config;
    }

    public function handle( string $message,$topic) {

        $destination=key_exists($topic,$this->config)?$topic:'default';
        $message= $this->config[$destination]['keywords']."\n".$message;
        $message = ["msgtype" => "text", "text" => ["content" => $message],
                    "at"      => ["atMobiles" => [], "isAtAll" => false]];
        $token=$this->config[$destination]['token'];
        $uri = '/robot/send?access_token=' . $token;
        $this->client->setMethod("POST");
        $this->client->setData(json_encode($message));
        $this->client->execute($uri);
        $return=$this->client->body;
        output('Ding请求:结果='.$return);
        $this->client->close();
        $res=json_decode($return,true);
        if(!is_array($res)||key_exists('error',$res)){
            throw new WarringException('发送钉钉消息失败:uri='.$uri.'信息='.json_encode($message).'结果='.$return);
        }else{
            $result=['status'=>true,'msg'=>'ok'];
        }
        return $result;
    }
}
