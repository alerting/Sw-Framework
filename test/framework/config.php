<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/6
 * Time: 11:27
 * description:描述
 */
namespace  Test\framework;

use Lc\lib\Env;
use PHPUnit\Framework\TestCase;
use Lc\lib\Config;

class EnvTest extends TestCase
{
    public function test()
    {
        Env::getInstance()->load(dirname(dirname(__DIR__)).'/.env');
        $config=Config::getInstance()->load(dirname(dirname(__DIR__)).'/app/common/config');

        $this->assertEquals('0.0.0.0', $config->get('app.host'));

    }
}
