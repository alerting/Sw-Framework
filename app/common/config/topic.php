<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2020/2/5
 * Time: 18:39
 * description:描述
 */

return [
    'topicConsumersMap'=>[
        'veinopen'=>['messager','db'],
        'veinVerify'=>['db',],
        'messager'=>['messager',],
    ],
    "topicProtocol" =>  [
        'veinopen' =>  ['logId', 'ip','host','method', 'uri', 'time', 'project', 'serverIp','ext'],
        'messager' => ['logId', 'ip', 'uri', 'time', 'project', 'serverIp', 'param','ext'],
        'veinVerify' =>  ['logId', 'action', 'time', 'authType','firm_id','firm_name','space_id','deviceId','deviceName','result','veinUid','user','score', 'mode','ext']
    ],

];
