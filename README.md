# LogOpen

一个基于swoole的简易框架.可供swoole学习者学习研究


## Environment
---
>1. PHP 版本不低于 PHP7.0;
>2. swoole 4.4.x  

## 目录结构
---
logCenter--入口文件
composer.json
.env.example-- 环境变量文件
test--测试
framework--框架核心
|--lib    库类
|--callback  swoole事件响应
|--framework.php   框架核心
|--helper.php   助手函数
app---业务代码
|---common/config  应用配置
|---log            模块