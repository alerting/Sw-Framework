<?php
/**
 * Created by PhpStorm.
 * User: 123456
 * Date: 2019/12/23
 * Time: 10:37
 * description:描述
 */

namespace App\log\controller;

use Lc\lib\redis\RedisPool;
use App\common\exception\ConfigException;
use App\common\exception\WarringException;


class LogController {
    const MESSAGE_QUEUE       = 'logEvent:queue:';
    const MESSAGE_DELAY_QUEUE = 'logEvent:zset:';
    private   $consumerConfig;
    protected $reporterConfig;
    private   $topicConsumersMap;
    private   $topicProtocol;
    private   $consumers;
    private   $redis;


    public function __construct() {
        $this->topicConsumersMap = config('topic.topicConsumersMap');
        $this->topicProtocol = config('topic.topicProtocol');
        $this->consumerConfig = config('comsumer');
        $this->reporterConfig = config('reporter');
    }

    public function consumeFromRedis(array $params) {

        if (!key_exists('topic', $params)) {
            throw new ConfigException('请求参数不存在topic');
        }
        $topic = $params['topic'];
        if (!key_exists($topic, $this->topicConsumersMap)) {
            throw new ConfigException('不存在的topic:' . $topic);
        }
        $this->initConsumers($topic);
        $this->redis = RedisPool::getInstance()->get();

        $redisTopicKey = self::MESSAGE_QUEUE . $topic;
        $num = 0;
        while (true) {
            $message = $this->redis->rPop($redisTopicKey);
            if (!$message || $num > 100) {
                break;
            }
            try {
                $num++;
                $this->messageHandles($message, $topic);
            } catch (\Throwable $e) {
                output('消息处理异常:文件' . $e->getFile() . ';第' . $e->getLine() . '行;错误信息' . $e->getMessage() . "内容:" . $message);
                $this->report('消息处理异常:' . $e->getMessage() . "\n内容:" . $message);
            }
        }
        return;
    }

    /**
     * 初始化消费者
     * @param array $consumersMap
     * @throws ConfigException
     */
    private function initConsumers(string $topic) {
        foreach ($this->topicConsumersMap[$topic] as $consumer) {
            $consumerName = 'App\log\consumer\\' . ucfirst($consumer);
            if (!class_exists($consumerName)) {
                throw new ConfigException('配置异常,消费者' . $consumerName . '不存在');
            }
            $this->consumers[] = new $consumerName(array_merge($this->consumerConfig[$consumer],['protocol'=>$this->topicProtocol[$topic]]));
        }
    }

    public function consumeFromRequest(string $topic, array $message) {

        if (!key_exists($topic, $this->topicConsumersMap)) {
            throw new ConfigException('不存在的topic:' . $topic);
        }
        $this->initConsumers($topic);
        $this->messageHandles(json_encode($message), $topic);
        return;
    }

    /**
     *
     * @param $message
     * @return mixed|void
     */
    private function messageHandles(string $message, string $topic) {

        $message = json_decode($message, true);
        //todo  日志协议检查
        $this->logProtocolCheck($message, $topic);

        output('message处理:消费者数量'.count($this->consumers) .'数据'. json_encode($message));

        $result = $this->messageFilter($message, function ($message) use ($topic) {
            $result = [];
            foreach ($this->consumers as $consumer) {
                try {
                    $consumerName = getClass($consumer);
                    output($consumerName . ' 消费信息:' . $message['logId']);
                    $res = $consumer->handle($message, $topic);
                    if (!$res['status']) {
                        throw new WarringException(" 消费信息失败,结果:" . $res);
                    }
                    $result[$consumerName] = 'success';
                } catch (\Throwable $e) {
                    $result[$consumerName] = 'fail';
                    output('消费异常:消费者:'.$consumerName.'异常信息:'.$e->getMessage());
                    $this->report('消费异常:消费者:'.$consumerName.'异常信息:'.$e->getMessage());
                }
            }
            return $result;
        });
        $this->resultHandle($result);
    }

    //todo 协议没有检查参数类型
    protected function logProtocolCheck(array $message, string $topic) {

        if ($lack = array_diff($this->topicProtocol[$topic], array_keys($message))) {
            throw new WarringException('不支持的日志协议:缺少关键key:' . implode(',', $lack));
        }
    }

    protected function messageFilter(array $message, \Closure $func) {

        return $func($message);
    }

    protected function resultHandle(array $result) {
        foreach ($result as $key => $value) {
            if ($value == 'fail') {
//                $this->redis->zAdd();
            }
        }
    }

    public function report(string $content) {
        reportLog($content, $this->reporterConfig);
    }

    function __destruct() {
        // TODO: Implement __destruct() method.
        unset($this->consumers);
        if ($this->redis)
            RedisPool::getInstance()->pushRedis($this->redis);
    }

}
